with GNAT.SHA256;  use GNAT.SHA256;
with Neighbor;     use Neighbor;
with Neighborhood; use Neighborhood;

package Self is

  procedure Initialize;
  --
  -- Initialize Self
  
  function Info return Neighborhood_Type;
  --
  -- Returns the Neighbor Info of Self
  
  --procedure Update_State (Neighbor    : in Message_Digest;
--                            State_Group : in State_Group_Type;
--                            State       : in State_Type;
--                            Requestor   : in Message_Digest);
--    --
  -- Update the State of the State_Group for the given Neighbor
  
--  function Hood_Iterator return Neighborhood.Cursor_Type;
  --
  -- Returns a Cursor for the First Neighbor in the Neighborhood
  
  --function Next_Neighbor (Neighbor : Cursor_Type) return Cursor_Type;
  --
  -- Increments to the Next_Neighbor in the Neighborhood
  
private
  
  This_Self : Neighborhood_Type;
  
end Self;
