with Ada.Strings.Fixed; use Ada.Strings;

package body Neighborhood is

  ------------
  -- Create --
  ------------

  overriding function Create (Name : String) return Neighborhood_Type is
    This : Neighborhood_Type;
  begin
    Neighbor_Type (This) := Neighbor.Create (Name);
    return This;
  end Create;

  ------------------
  -- Add_Neighbor --
  ------------------

  procedure Add_Neighbor (Object   : in out Neighborhood_Type;
                          Neighbor : in     Neighbor_Type) is
  begin
    Neighbor_List.Add (Object.Neighborhood, Neighbor);
  end Add_Neighbor;

  ---------------------
  -- Remove_Neighbor --
  ---------------------

  procedure Remove_Neighbor (Object   : in out Neighborhood_Type;
                             Neighbor : in     Neighbor_Type) is
  begin
    Neighbor_List.Find (Object.Neighborhood, Neighbor);
    Neighbor_List.Remove (Object.Neighborhood);
  end Remove_Neighbor;

  ---------------------
  -- Neighbor_Exists --
  ---------------------

  function Neighbor_Exists (Object   : Neighborhood_Type;
                            Neighbor : Message_Digest) return Boolean is
    New_Neighbor : Neighbor_Type;
  begin
    New_Neighbor := Create_Dummy (Neighbor);
    return Neighbor_List.Exists (Object.Neighborhood, New_Neighbor);
  end Neighbor_Exists;

  --------------
  -- Iterator --
  --------------

  function Iterator (Object : Neighborhood_Type)
                     return Neighbor_List.Cursor_Type is
  begin
    return Neighbor_List.First (Object.Neighborhood);
  end Iterator;

  ----------
  -- Next --
  ----------

  function Next (Object : Neighbor_List.Cursor_Type)
                 return Neighbor_List.Cursor_Type is
    Null_Cursor : Neighbor_List.Cursor_Type;
  begin
    return Neighbor_List.Next (Object);
  exception
    when Neighbor_List.ELEMENT_NOT_FOUND =>
      return Null_Cursor;
  end Next;

  -------------
  -- Private --
  -------------

  --------------------
  -- Dummy_Neighbor --
  --------------------

  function Create_Dummy (Unique_Id : Message_Digest) return Neighborhood_Type is
    Neighborhood : Neighborhood_Type;
  begin
    Neighbor_Type (Neighborhood) := Create_Dummy (Unique_Id);
    return Neighborhood;
  end Create_Dummy;


end Neighborhood;
