with GNAT.SHA256; use GNAT.SHA256;

package State_Group is

  type State_Group_Type is tagged private;
  
  function Create (Name : String) return State_Group_Type;
  --
  -- Creates a new State_Group
  
  function Create_Dummy (Unique_Id : Message_Digest) return State_Group_Type;
  --
  -- Creates a State_Group shell that only has a Unique_Id.  This is intended to
  -- be used to create a State_Group to check if it exists in the neighbor.
  
  function Name (State_Group : State_Group_Type) return Message_Digest;
  --
  -- Returns a Hash of the State_Group Name
  
  function Current_State (State_Group : State_Group_Type) return Message_Digest;
  function Current_State (State_Group : Message_Digest) return Message_Digest;
  --
  -- Returns a Hash of the Current State for the State_Group
  
  procedure Set_State (State_Group : in out State_Group_Type;
                       State       : in     Message_Digest);
  --
  -- Set the Current State of the State_Group
  
private
  
  type State_Group_Type is tagged record
    Name          : Message_Digest;
    Current_State : Message_Digest;
  end record;
  
end State_Group;
