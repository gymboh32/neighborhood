package body Basic_List is

  -----------
  -- First --
  -----------

  function First (Cursor : Cursor_Type) return Cursor_Type is
    First_Cursor : Cursor_Type := Cursor;
  begin
    while First_Cursor.Prev /= null loop
      First_Cursor := First_Cursor.Prev;
    end loop;
    return First_Cursor;
  end First;

  ----------
  -- Last --
  ----------

  function Last (Cursor : Cursor_Type) return Cursor_Type is
    Last_Cursor : Cursor_Type := Cursor;
  begin
    while Last_Cursor.Next /= null loop
      Last_Cursor := Last_Cursor.Next;
    end loop;
    return Last_Cursor;
  end Last;

  ------------
  -- Create --
  ------------

  function Create return Cursor_Type is
    List : Cursor_Type := new List_Type;
  begin
    List.Prev := null;
    List.Next := null;
    return List;
  end Create;

  ---------
  -- Add --
  ---------

  procedure Add (Cursor  : in out Cursor_Type;
                 Element : in     Element_Type) is
    Temp_Cursor : Cursor_Type := New List_Type;
  begin
    -- Set the element
    Temp_Cursor.This := Element;
    -- Link the Previous Element to the current cursor
    Temp_Cursor.Prev := Cursor;
    -- Link the Next Element to the Next Element of the current cursor
    Temp_Cursor.Next := Cursor.Next;
    -- Link the Next Element of the current cursor to the new Element
    Cursor.Next      := Temp_Cursor;
  end Add;

  ------------
  -- Remove --
  ------------

  procedure Remove (Cursor : in out Cursor_Type) is
  begin
    if Is_Empty (Cursor) then
      raise ELEMENT_NOT_FOUND;
    end if;
    if Cursor.Next /= null then
      Cursor := Cursor.Next;
      if Cursor.Prev.Prev /= null then
        Cursor.Prev := Cursor.Prev.Prev;
      else
        Cursor.Prev := null;
      end if;
    elsif Cursor.Prev /= null then
      Cursor := Cursor.Prev;
      if Cursor.Next.Next /= null then
        Cursor.Next := Cursor.Next.Next;
      else
        Cursor.Next := null;
      end if;
    else
      Cursor := null;
    end if;
  end Remove;

  ----------
  -- This --
  ----------

  function This (Cursor : Cursor_Type) return Element_Type is
  begin
    if Cursor = null then
      raise ELEMENT_NOT_FOUND;
    end if;
    return Cursor.This;
  end This;

  ----------
  -- Prev --
  ----------

  function Prev (Cursor : Cursor_Type) return Cursor_Type is
  begin
    return Cursor.Prev;
  end Prev;

  ----------
  -- Next --
  ----------

  function Next (Cursor : Cursor_Type) return Cursor_Type is
  begin
    return Cursor.Next;
  end Next;

  -------------
  -- Is_Null --
  -------------

  function Is_Null (Cursor : Cursor_Type) return Boolean is
  begin
    return Cursor = null;
  end Is_Null;

  --------------
  -- Is_Empty --
  --------------

  function Is_Empty (Cursor : Cursor_Type) return Boolean is
    C : Cursor_Type := First (Cursor);
  begin
    return Is_Null (Cursor);
  end Is_Empty;

  ------------
  -- Exists --
  ------------

  function Exists (Cursor  : Cursor_Type;
                   Element : Element_Type) return Boolean is
    Temp_Cursor    : Cursor_Type := First (Cursor);
    Element_Exists : Boolean     := False;
  begin
    while Temp_Cursor /= null loop
      Element_Exists := This (Temp_Cursor) = Element;
      Temp_Cursor    := Temp_Cursor.Next;
    end loop;
    return Element_Exists;
  end Exists;

  ----------
  -- Find --
  ----------

  procedure Find (Cursor  : in out Cursor_Type;
                  Element : in     Element_Type) is
  begin
    Cursor := FIrst (Cursor);
    while Cursor /= null loop
      exit when Cursor.This = Element;
      Cursor := Cursor.Next;
    end loop;
    if Cursor.This /= Element then
      raise ELEMENT_NOT_FOUND;
    end if;
  end Find;

end Basic_List;
