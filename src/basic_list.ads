generic 
   type Element_Type is private;
package Basic_List is

  ELEMENT_NOT_FOUND : exception;
  
  type Cursor_Type is private;
  
  function Create return Cursor_Type;
  --
  -- Creates a Basic List
  
  procedure Add (Cursor  : in out Cursor_Type;
                 Element : in     Element_Type);
  --
  -- Adds an Element to the Next Cursor position
  
  procedure Remove (Cursor : in out Cursor_Type);
  --
  -- Removes the Element at the Cursor position from the List
  -- Exception: ELEMENT_NOT_FOUND

  function This (Cursor : Cursor_Type) return Element_Type;
  --
  -- Returns the Element at the Cursor position
  -- Exception: ELEMENT_NOT_FOUND
  
  function Prev (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the Cursor at the previous position
  -- Exception ELEMENT_NOT_FOUND
  
  function Next (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the Cursor at the next position
  -- Exception ELEMENT_NOT_FOUND
  
  function Is_Null (Cursor : Cursor_Type) return Boolean;
  --
  -- Returns True if the Cursor is Null
  
  function Is_Empty (Cursor : Cursor_Type) return Boolean;
  --
  -- Returns True if the whole List is Empty
  
  function Exists (Cursor  : Cursor_Type;
                   Element : Element_Type) return Boolean;
  --
  -- Returns True if the Element Exists
  
  procedure Find (Cursor  : in out Cursor_Type;
                  Element : in     Element_Type);
  --
  -- Sets the cursor to the given Element.
  -- Exception: ELEMENT_NOT_FOUND
  
  function First (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the First Element in the List
  -- Exception: ELEMENT_NOT_FOUND
  
  function Last (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the Last Element in the list
  -- Exception: ELEMENT_NOT_FOUND
  
private
    
  type List_Type is record
    This : Element_Type;
    Prev : Cursor_Type;
    Next : Cursor_Type;
  end record;
    
  type Cursor_Type is access List_Type;
  
end Basic_List;
