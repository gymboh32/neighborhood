with Ada.Text_IO; use Ada.Text_IO;
with Comms;
with Self;

procedure Main is
begin
  Put_Line ("Hello, World!");

  -- Task for Initializing Self
  -- Task for Initializing Comms
  -- Task to Execute Self
  -- Task to Listen for Messages
  -- Everything can be a single threaded infinite loop for now

end Main;
