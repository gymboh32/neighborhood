with Basic_List;
with GNAT.SHA256; use GNAT.SHA256;
with Neighbor;    use Neighbor;

package Neighborhood is
  
  package Neighbor_List is new Basic_List (Neighbor_Type);
  
  type Neighborhood_Type is new Neighbor_Type with private;
  
  overriding function Create (Name : String) return Neighborhood_Type;
  --
  -- Creates a new Neighborhood
  
  procedure Add_Neighbor (Object   : in out Neighborhood_Type;
                          Neighbor : in     Neighbor_Type);
  --
  -- Adds a Neighbor to Self's Neighborhood
  
  procedure Remove_Neighbor (Object   : in out Neighborhood_Type;
                             Neighbor : in     Neighbor_Type);
  --
  -- Removes a Neighbor from Self's Neighborhood
  
  function Neighbor_Exists (Object   : Neighborhood_Type;
                            Neighbor : Message_Digest) return Boolean;
  --
  -- Returns True if the Neighbor exists

  function Iterator (Object : Neighborhood_Type) 
                     return Neighbor_List.Cursor_Type;
  --
  -- Returns a Cursor at the start of the Neighborhood
  -- Null is returned if the neighborhood is empty
  
  function Next (Object : Neighbor_List.Cursor_Type) 
                 return Neighbor_List.Cursor_Type;
  --
  -- Returns the Next Neighbor in the Neighborhood
  -- Null is returned at the end of the list.
  
private
  
  -- Extends Neighbor_Type
  -- Extends Trust_Type
  type Neighborhood_Type is new Neighbor_Type with record
    Neighborhood : Neighbor_List.Cursor_Type;
  end record;
  
  overriding 
  function Create_Dummy (Unique_Id : Message_Digest) return Neighborhood_Type;
  
end Neighborhood;
  
