with GNAT.SHA256; use GNAT.SHA256;

package Comms is

  type Header_Type is record
    To   : Message_Digest;
    From : Message_Digest;
    -- Confidence : Natural;
  end record;

  type Data_Type is tagged record
    Neighbor_Id : Message_Digest;
    State_Group : Message_Digest;
    State       : Message_Digest;
  end record;
  
  --    type Footer_Type is record
  --      Checksum : Message_Digest;
  --    end record;
 
  type Message_Type is record
    Header : Header_Type;
    Data   : Data_Type;
    --      Footer : Footer_Type;
  end Record;
  
  function Create_Message (To          : Message_Digest;
                           Neighbor_Id : Message_Digest;
                           State_Group : Message_Digest;
                           State       : Message_Digest) return Message_Type;
  --
  -- Creates an Update Message
    
  procedure Send (Message : in Message_Type);
  --
  -- Sends a Message
  
  procedure Receive;
  --
  -- Receives a message
  
end Comms;
