package Trust is

  subtype Name_String is String (1 .. 80);
  
  type Trust_Type is tagged private;
  
  function Create (Name : String) return Trust_Type;
  --
  -- Creates an Object of Trust
  
  procedure Add_Trust (Object : in out Trust_Type);
  --
  -- Adds a Level of Trust from the Object
  
  procedure Remove_Trust (Object : in out Trust_Type);
  --
  -- Removes a Level of Trust from the Object
  
  function Level (Object : in Trust_Type) return Float;
  --
  -- Returns the Level of Trust in the Object
  
  function Name (Object : Trust_Type) return Name_String;
  --
  -- Returns the Name of the Neighbor
  
private
  
  type Trust_Type is tagged record
    Name         : Name_String;
    Trust        : Natural;
    Distrust     : Natural;
    Wilson_Score : Float;
  end record;
  
end Trust;
