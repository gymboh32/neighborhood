with Basic_List;
with GNAT.SHA256; use GNAT.SHA256;
with State_Group; use State_Group;
with Trust;       use Trust;

package Neighbor is

  package State_Group_List is new Basic_List (State_Group_Type);

  type Neighbor_Type is new Trust_Type with private;
  
  function "=" (Left  : Neighbor_Type;
                Right : Neighbor_Type) return Boolean;
  --
  -- Returns True if Left = Right

  overriding
  function Create (Name : String) return Neighbor_Type;
  --
  -- Creates a new Neighbor

  function Unique_Id (Object : Neighbor_Type) return Message_Digest;
  --
  -- Returns the Unique Identifier of the Neighbor
  
  function State_Group (Object : Neighbor_Type;
                        Group  : Message_Digest) return State_Group_Type;
  --
  -- Returns the requested State_Group

  function Create_Dummy (Unique_Id : Message_Digest) return Neighbor_Type;
  --
  -- Creates a neighbor shell that only has a Unique_Id.  This is intended to be
  -- used to create a neighbor to check if it exists in the neighborhood.
    
private
      
  -- Extends Trust_Type
  type Neighbor_Type is new Trust_Type with record
    Unique_Id  : Message_Digest;
    Group_List : State_Group_List.Cursor_Type;
  end record;
  
end Neighbor;
