with GNAT.Sockets; use GNAT.Sockets;
with Neighbor;     use Neighbor;
with Neighborhood; use Neighborhood;
with Self;
with State_Group;  use State_Group;

package body Comms is

  --------------------
  -- Create_Message --
  --------------------

  function Create_Message
    (To          : Message_Digest;
     Neighbor_Id : Message_Digest;
     State_Group : Message_Digest;
     State       : Message_Digest) return Message_Type is
    Header  : Header_Type;
    Data    : Data_Type;
    Message : Message_Type;
  begin
    Header := (To   => To,
               From => Self.Info.Unique_Id);
    Data := (Neighbor_Id => Neighbor_Id,
             State_Group => State_Group,
             State       => State);
    Message := (Header => Header,
                Data   => Data);

    return Message;

  end Create_Message;

  -----------
  --- Send --
  -----------

  procedure Send (Message : in Message_Type) is
    Address : Sock_Addr_Type;
    Socket  : Socket_Type;
    Channel : Stream_Access;
  begin
    -- TODO: Update to send to intended neighbor
    Address.Addr := Addresses (Get_Host_By_Name (Host_Name), 1);
    Address.Port := 8484;
    Create_Socket (Socket);

    Connect_Socket (Socket, Address);

    --  Connected, use a stream connected to the socket

    Channel := Stream (Socket);

    -- Send the Message
    Message_Type'Output (Channel, Message);

  end Send;

  -------------
  -- Receive --
  -------------

  procedure Receive is
    Address : Sock_Addr_Type;
    Socket  : Socket_Type;
    Server  : Socket_Type;
    Channel : Stream_Access;
    Message : Message_Type;
  begin

    -- Obtain the message

    Address.Addr := Addresses (Get_Host_By_Name (Host_Name), 1);
    Address.Port := 8484;

    Create_Socket (Server);

    Bind_Socket (Server, Address);

    Listen_Socket (Server);

    Accept_Socket (Server, Socket, Address);

    --  A client has been accepted, get the stream connected to the socket

    Channel := Stream (Socket);

    Message := Message_Type'Input (Channel);

    declare
      Neighbor_State_Group : State_Group_Type;
    begin

      -- Check Neighborhood for 'From'
      if Self.Info.Neighbor_Exists (Message.Header.From) then
        -- check Neighborhood for Neighbor_Id
        if Self.Info.Neighbor_Exists (Message.Data.Neighbor_Id) then
            Neighbor_State_Group := Neighbor_List.This
              (Self.Info.Cursor (Message.Data.Neighbor_Id))
                .State_Group (Message.Data.State_Group);
            -- Compare State_Group State with State_Group Current_State
          if Message.Data.State /= Current_State (Neighbor_State_Group) then
            -- Update State
            Set_State (State_Group => Neighbor_State_Group,
                       State       => Message.Data.State);
            -- Distribute Message
            declare
              Cursor      : Neighbor_List.Cursor_Type := Self.Info.Cursor;
              New_Message : Message_Type;
              use type Neighbor_List.Cursor_Type;
            begin
              while not Neighbor_List.Is_Null (Cursor) loop
                New_Message := Create_Message
                  (To          => Neighbor_List.This (Cursor).Unique_Id,
                   Neighbor_Id => Message.Data.Neighbor_Id,
                   State_Group => Message.Data.State_Group,
                   State       => Message.Data.State);
                Send (New_Message);
                Cursor := Neighbor_List.Next (Cursor);
              end loop;
            end;
          end if;
        end if;
      end if;
    end;
  end Receive;

end Comms;
