with Ada.Strings.Fixed; use Ada.Strings;
with GNAT.Time_Stamp; use GNAT.Time_Stamp;

package body Neighbor is

  -----------
  -- LOCAL --
  -----------

  ----------
  -- Hash --
  ----------

  function Hash (Name : String) return Message_Digest is
    C : Context := Initial_Context;
  begin
    -- Using the Name and the Time Stamp of creation should be unique enough
    Update (C, Name);
    Update (C, Current_Time);
    return Digest (C);
  end Hash;

  ------------
  -- PUBLIC --
  ------------

  function "=" (Left  : Neighbor_Type;
                Right : Neighbor_Type) return Boolean is
  begin
    return Left.Unique_Id = Right.Unique_Id;
  end "=";

  ------------
  -- Create --
  ------------

  function Create (Name : String) return Neighbor_Type is
    Neighbor : Neighbor_Type;
  begin
    Trust_Type (Neighbor) := Trust.Create (Name);
    Neighbor.Unique_Id    := Hash (Name);
    Neighbor.Add_Trust;
    return Neighbor;
  end Create;

  ---------------
  -- Unique_Id --
  ---------------

  function Unique_Id (Object : Neighbor_Type) return Message_Digest is
  begin
    return Object.Unique_Id;
  end Unique_Id;

  -----------------
  -- State_Group --
  -----------------

  function State_Group (Object : Neighbor_Type;
                        Group  : Message_Digest) return State_Group_Type is
    Temp_Group  : State_Group_Type;
    Temp_Object : Neighbor_Type := Object;
  begin
    Temp_Group := Create_Dummy (Group);
    State_Group_List.Find (Temp_Object.Group_List, Temp_Group);
    return State_Group_List.This (Temp_Object.Group_List);
  end State_Group;

  -----------------
  -- Creat_Dummy --
  -----------------

  function Create_Dummy (Unique_Id : Message_Digest) return Neighbor_Type is
    Neighbor : Neighbor_Type;
  begin
    Trust_Type (Neighbor) := Trust.Create ("Dummy");
    Neighbor.Unique_Id := Unique_Id;
    return Neighbor;
  end Create_Dummy;


end Neighbor;
