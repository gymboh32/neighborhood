# Neighborhood

Proof of Concept for a framework for a Network Of Trust for Internet Of Things connected devices.

A decentralized way of issuing actions and updating states within a network.

All Devices talk to each other directly.  The message received is weighted based on the wilson score of the sending neighbor, the  
height of the message, and number of neighbors in consensus.  If a message's score reaches a certain threshold it is accepted.  
Any Neighbors that agreed when a message is accepted are given a vote of Trust, any neighbor that disagreed with the consensus is  
given a vote of Distrust.  When a Neighbor's Score drops below a threshold it is dropped from the neighborhood, leaving room for another  
Neighbor to be added.  If a message is received from an unknown Neighbor it is ignored unless there is room in the Neighborhood.  At this  
point the Neighbor is added to the Neighborhood and given a (small) chance to gain Trust.  When Neighbors are deliberately paired they  
are given a vote of Trust and thus weighted higher. 

 - This Framework allows an embedded device to be implemented without having to care about communicating over an insecure network.  
 - Commands are not accepted and States of known Neighbors are not updated without a high level of Trust.  
 - States are propogated over a network without direct exposure as to what those states are.
 - Trust has to be earned, preventing any Device from entering the network.
 - Each device in the network is independent and open to make and break relationships with neighbors as needed.

